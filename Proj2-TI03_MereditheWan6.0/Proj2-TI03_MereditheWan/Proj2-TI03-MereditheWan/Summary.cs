﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_TI03_MereditheWan
{
    public partial class summaryForm : Form
    {
        decimal subtotalDec, totalDiscountDec, totalDec;

        public summaryForm(decimal subtotalDecimal, decimal totalDiscountDecimal)
        {
            InitializeComponent();

            subtotalDec = subtotalDecimal;
            totalDiscountDec = totalDiscountDecimal;
            totalDec = subtotalDec - totalDiscountDec;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void discountLabel_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void summaryForm_Load(object sender, EventArgs e)
        {
            subtotalTextBox.Text = subtotalDec.ToString("C");
            discountTextBox.Text = totalDiscountDec.ToString("C");
            totalTextBox.Text = totalDec.ToString("C");
        }
    }
}
