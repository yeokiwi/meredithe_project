﻿namespace Proj2_TI03_MereditheWan
{
    partial class BusinessReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.transactionsLabel = new System.Windows.Forms.Label();
            this.totalSalesLabel = new System.Windows.Forms.Label();
            this.popGoProLabel = new System.Windows.Forms.Label();
            this.transactionTextBox = new System.Windows.Forms.TextBox();
            this.totalSalesTextBox = new System.Windows.Forms.TextBox();
            this.popAddOnTextBox = new System.Windows.Forms.TextBox();
            this.closeButton2 = new System.Windows.Forms.Button();
            this.popAddOnLabel = new System.Windows.Forms.Label();
            this.popGoProTextBox = new System.Windows.Forms.TextBox();
            this.totalGoProTextBox = new System.Windows.Forms.TextBox();
            this.totalGoProLabel = new System.Windows.Forms.Label();
            this.dailyBusinessReportGroupBox = new System.Windows.Forms.GroupBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.staffAccessGroupBox = new System.Windows.Forms.GroupBox();
            this.enterButton = new System.Windows.Forms.Button();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.maintenanceTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dailyBusinessReportGroupBox.SuspendLayout();
            this.staffAccessGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // transactionsLabel
            // 
            this.transactionsLabel.AutoSize = true;
            this.transactionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transactionsLabel.Location = new System.Drawing.Point(19, 60);
            this.transactionsLabel.Name = "transactionsLabel";
            this.transactionsLabel.Size = new System.Drawing.Size(232, 29);
            this.transactionsLabel.TabIndex = 0;
            this.transactionsLabel.Text = "No. Of Transactions:";
            // 
            // totalSalesLabel
            // 
            this.totalSalesLabel.AutoSize = true;
            this.totalSalesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalSalesLabel.Location = new System.Drawing.Point(19, 110);
            this.totalSalesLabel.Name = "totalSalesLabel";
            this.totalSalesLabel.Size = new System.Drawing.Size(243, 29);
            this.totalSalesLabel.TabIndex = 1;
            this.totalSalesLabel.Text = "Total Sales Revenue:";
            // 
            // popGoProLabel
            // 
            this.popGoProLabel.AutoSize = true;
            this.popGoProLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popGoProLabel.Location = new System.Drawing.Point(19, 219);
            this.popGoProLabel.Name = "popGoProLabel";
            this.popGoProLabel.Size = new System.Drawing.Size(237, 29);
            this.popGoProLabel.TabIndex = 2;
            this.popGoProLabel.Text = "Most Popular GoPro:";
            // 
            // transactionTextBox
            // 
            this.transactionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transactionTextBox.Location = new System.Drawing.Point(432, 60);
            this.transactionTextBox.Name = "transactionTextBox";
            this.transactionTextBox.ReadOnly = true;
            this.transactionTextBox.Size = new System.Drawing.Size(174, 35);
            this.transactionTextBox.TabIndex = 3;
            this.transactionTextBox.TabStop = false;
            this.transactionTextBox.TextChanged += new System.EventHandler(this.transactionTextBox_TextChanged);
            // 
            // totalSalesTextBox
            // 
            this.totalSalesTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalSalesTextBox.Location = new System.Drawing.Point(432, 110);
            this.totalSalesTextBox.Name = "totalSalesTextBox";
            this.totalSalesTextBox.ReadOnly = true;
            this.totalSalesTextBox.Size = new System.Drawing.Size(174, 35);
            this.totalSalesTextBox.TabIndex = 4;
            this.totalSalesTextBox.TabStop = false;
            // 
            // popAddOnTextBox
            // 
            this.popAddOnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popAddOnTextBox.Location = new System.Drawing.Point(432, 275);
            this.popAddOnTextBox.Name = "popAddOnTextBox";
            this.popAddOnTextBox.ReadOnly = true;
            this.popAddOnTextBox.Size = new System.Drawing.Size(174, 35);
            this.popAddOnTextBox.TabIndex = 5;
            this.popAddOnTextBox.TabStop = false;
            // 
            // closeButton2
            // 
            this.closeButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton2.Location = new System.Drawing.Point(231, 385);
            this.closeButton2.Name = "closeButton2";
            this.closeButton2.Size = new System.Drawing.Size(148, 41);
            this.closeButton2.TabIndex = 1;
            this.closeButton2.Text = "Close";
            this.closeButton2.UseVisualStyleBackColor = true;
            // 
            // popAddOnLabel
            // 
            this.popAddOnLabel.AutoSize = true;
            this.popAddOnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popAddOnLabel.Location = new System.Drawing.Point(19, 275);
            this.popAddOnLabel.Name = "popAddOnLabel";
            this.popAddOnLabel.Size = new System.Drawing.Size(250, 29);
            this.popAddOnLabel.TabIndex = 8;
            this.popAddOnLabel.Text = "Most Popular Add-On:";
            // 
            // popGoProTextBox
            // 
            this.popGoProTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popGoProTextBox.Location = new System.Drawing.Point(432, 219);
            this.popGoProTextBox.Name = "popGoProTextBox";
            this.popGoProTextBox.ReadOnly = true;
            this.popGoProTextBox.Size = new System.Drawing.Size(174, 35);
            this.popGoProTextBox.TabIndex = 9;
            this.popGoProTextBox.TabStop = false;
            // 
            // totalGoProTextBox
            // 
            this.totalGoProTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalGoProTextBox.Location = new System.Drawing.Point(432, 163);
            this.totalGoProTextBox.Name = "totalGoProTextBox";
            this.totalGoProTextBox.ReadOnly = true;
            this.totalGoProTextBox.Size = new System.Drawing.Size(174, 35);
            this.totalGoProTextBox.TabIndex = 10;
            this.totalGoProTextBox.TabStop = false;
            // 
            // totalGoProLabel
            // 
            this.totalGoProLabel.AutoSize = true;
            this.totalGoProLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalGoProLabel.Location = new System.Drawing.Point(19, 163);
            this.totalGoProLabel.Name = "totalGoProLabel";
            this.totalGoProLabel.Size = new System.Drawing.Size(218, 29);
            this.totalGoProLabel.TabIndex = 11;
            this.totalGoProLabel.Text = "Total GoPros Sold:";
            // 
            // dailyBusinessReportGroupBox
            // 
            this.dailyBusinessReportGroupBox.Controls.Add(this.label1);
            this.dailyBusinessReportGroupBox.Controls.Add(this.maintenanceTextBox);
            this.dailyBusinessReportGroupBox.Controls.Add(this.transactionsLabel);
            this.dailyBusinessReportGroupBox.Controls.Add(this.totalGoProLabel);
            this.dailyBusinessReportGroupBox.Controls.Add(this.totalSalesLabel);
            this.dailyBusinessReportGroupBox.Controls.Add(this.totalGoProTextBox);
            this.dailyBusinessReportGroupBox.Controls.Add(this.popGoProLabel);
            this.dailyBusinessReportGroupBox.Controls.Add(this.popGoProTextBox);
            this.dailyBusinessReportGroupBox.Controls.Add(this.transactionTextBox);
            this.dailyBusinessReportGroupBox.Controls.Add(this.popAddOnLabel);
            this.dailyBusinessReportGroupBox.Controls.Add(this.totalSalesTextBox);
            this.dailyBusinessReportGroupBox.Controls.Add(this.popAddOnTextBox);
            this.dailyBusinessReportGroupBox.Controls.Add(this.closeButton2);
            this.dailyBusinessReportGroupBox.Enabled = false;
            this.dailyBusinessReportGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dailyBusinessReportGroupBox.Location = new System.Drawing.Point(57, 224);
            this.dailyBusinessReportGroupBox.Name = "dailyBusinessReportGroupBox";
            this.dailyBusinessReportGroupBox.Size = new System.Drawing.Size(630, 432);
            this.dailyBusinessReportGroupBox.TabIndex = 17;
            this.dailyBusinessReportGroupBox.TabStop = false;
            this.dailyBusinessReportGroupBox.Text = "Daily Business Report";
            this.dailyBusinessReportGroupBox.Visible = false;
            // 
            // staffAccessGroupBox
            // 
            this.staffAccessGroupBox.Controls.Add(this.enterButton);
            this.staffAccessGroupBox.Controls.Add(this.passwordTextBox);
            this.staffAccessGroupBox.Controls.Add(this.passwordLabel);
            this.staffAccessGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staffAccessGroupBox.Location = new System.Drawing.Point(57, 26);
            this.staffAccessGroupBox.Name = "staffAccessGroupBox";
            this.staffAccessGroupBox.Size = new System.Drawing.Size(630, 158);
            this.staffAccessGroupBox.TabIndex = 18;
            this.staffAccessGroupBox.TabStop = false;
            this.staffAccessGroupBox.Text = "Staff Access";
            // 
            // enterButton
            // 
            this.enterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enterButton.Location = new System.Drawing.Point(458, 102);
            this.enterButton.Name = "enterButton";
            this.enterButton.Size = new System.Drawing.Size(120, 31);
            this.enterButton.TabIndex = 16;
            this.enterButton.Text = "Enter";
            this.enterButton.UseVisualStyleBackColor = true;
            this.enterButton.Click += new System.EventHandler(this.enterButton_Click);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(307, 41);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(271, 39);
            this.passwordTextBox.TabIndex = 15;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordLabel.Location = new System.Drawing.Point(46, 41);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(126, 29);
            this.passwordLabel.TabIndex = 12;
            this.passwordLabel.Text = "Password:";
            // 
            // maintenanceTextBox
            // 
            this.maintenanceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maintenanceTextBox.Location = new System.Drawing.Point(432, 332);
            this.maintenanceTextBox.Name = "maintenanceTextBox";
            this.maintenanceTextBox.ReadOnly = true;
            this.maintenanceTextBox.Size = new System.Drawing.Size(174, 35);
            this.maintenanceTextBox.TabIndex = 12;
            this.maintenanceTextBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 332);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(404, 29);
            this.label1.TabIndex = 13;
            this.label1.Text = "Most Popular Maintenance Package:";
            // 
            // BusinessReport
            // 
            this.AcceptButton = this.enterButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton2;
            this.ClientSize = new System.Drawing.Size(735, 686);
            this.Controls.Add(this.staffAccessGroupBox);
            this.Controls.Add(this.dailyBusinessReportGroupBox);
            this.Name = "BusinessReport";
            this.Text = "Business Report";
            this.Load += new System.EventHandler(this.BusinessReport_Load);
            this.dailyBusinessReportGroupBox.ResumeLayout(false);
            this.dailyBusinessReportGroupBox.PerformLayout();
            this.staffAccessGroupBox.ResumeLayout(false);
            this.staffAccessGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label transactionsLabel;
        private System.Windows.Forms.Label totalSalesLabel;
        private System.Windows.Forms.Label popGoProLabel;
        private System.Windows.Forms.TextBox transactionTextBox;
        private System.Windows.Forms.TextBox totalSalesTextBox;
        private System.Windows.Forms.TextBox popAddOnTextBox;
        private System.Windows.Forms.Button closeButton2;
        private System.Windows.Forms.Label popAddOnLabel;
        private System.Windows.Forms.TextBox popGoProTextBox;
        private System.Windows.Forms.TextBox totalGoProTextBox;
        private System.Windows.Forms.Label totalGoProLabel;
        private System.Windows.Forms.GroupBox dailyBusinessReportGroupBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox staffAccessGroupBox;
        private System.Windows.Forms.Button enterButton;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox maintenanceTextBox;
        private System.Windows.Forms.Label label1;
    }
}