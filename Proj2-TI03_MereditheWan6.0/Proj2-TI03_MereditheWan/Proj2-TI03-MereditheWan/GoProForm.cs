﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Proj2_TI03_MereditheWan
{
   
    public partial class GoProForm : Form
    {

        int orderInteger;

        List<Order> orderList = new List<Order>();
        List<decimal> transactionList = new List<decimal>();
        int[] goproArray = { 0, 0, 0, 0, 0, 0 };
        int[] addOnArray = { 0, 0, 0 };
        int[] maintenanceArray = { 0, 0, 0 };
        decimal currentDiscPrice;
        decimal cameraPriceDecimal, addOnsPriceDecimal1, addOnsPriceDecimal2, addOnsPriceDecimal3, maintenancePriceDecimal;
        decimal promoDecimal, totalAfterDiscountDecimal, totalSalesDecimal, totalAddOnsPriceDecimal, totalCameraPriceDecimal, totalMaintenancePriceDecimal;

        string cameraName, maintenanceName;
        string addOnsName1, addOnsName2, addOnsName3;
        int itemInteger = 0;
        int int8Black, int7Black, int7White, int7Silver, int6Black, int5Black, int3Way, intCasey, intSdCard;

        private void clearForNextItem()
        {
            invisibleRadioButton.Checked = true;
            noSelectionRadioButton.Checked = true;

            hero8BlackRadioButton.Checked = false;
            hero7SilverRadioButton.Checked = false; 
            hero6BlackRadioButton.Checked = false;
            hero7BlackRadioButton.Checked = false;
            hero7WhiteRadioButton.Checked = false;
            hero5BlackRadioButton.Checked = false;

            threeWayCheckBox.Checked = false;
            caseyCheckBox.Checked = false;
            sdCardCheckBox.Checked = false;

            basicPackageRadioButton.Checked = false;
            advancedPackageRadioButton.Checked = false;
            premiumPackageRadioButton.Checked = false;


        }
        public class Order
        {
            public int Model { get; set; }
            public bool ThreeWay { get; set; }

            public bool Casey { get; set; }
            public bool SDCard { get; set; }


            public int Maintenance { get; set; }
            public decimal Price { get; set; }

            public string Model_name { get; set; }
            public decimal Model_cost { get; set; }
            public decimal ThreeWay_cost { get; set; }
            public decimal SDCard_cost { get; set; }
            public decimal Casey_cost { get; set; }
            public decimal Maintenance_cost { get; set; }

            public Order(int model, bool threeway, bool casey, bool sdcard, int maintenance, decimal price)
            {
                Model = model;
                ThreeWay = threeway;
                Casey = casey;
                SDCard = sdcard;
                Maintenance = maintenance;
                Price = price;
            }

            public Order(int model,
                                string model_name,
                                decimal model_cost,
                                bool threeway,
                                decimal threeway_cost,
                                bool casey,
                                decimal casey_cost,
                                bool sdcard,
                                decimal sdcard_cost,
                                int maintenance,
                                decimal maintenance_cost,
                                decimal price)
            {
                Model = model;
                ThreeWay = threeway;
                Casey = casey;
                Casey_cost = casey_cost;
                SDCard = sdcard;
                Maintenance = maintenance;
                Price = price;
                Model_name = model_name;
                Model_cost = model_cost;
                ThreeWay_cost = threeway_cost;
                SDCard_cost = sdcard_cost;
                Maintenance_cost = maintenance_cost;
            }

        }

        public GoProForm()
        {
            InitializeComponent();
        }

        private void hero8BlackRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cameraPriceDecimal = 545.0m;
            cameraName = "Hero 8 Black";

            priceTextBox.Text = cameraPriceDecimal.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Hero_8_Black;

            addOnsGroupBox.Enabled = true;
            maintenancePackageGroupBox.Enabled = true;
            promotionCodeTextBox.Enabled = true;
            addToOrderButton.Enabled = true;

            specificationRichTextBox.Text = "Weight: 126g" + Environment.NewLine + "Burst: Yes" + Environment.NewLine
               + "Video: 4K60" + Environment.NewLine + "Battery: Removable" + Environment.NewLine +
               "Waterproof: 10m" + Environment.NewLine + "Touchscreen: Yes" + Environment.NewLine +
               "Stabilisation: HyperSmooth 2.0" + Environment.NewLine + "Digital lenses: SuperView, Wide, Linear, Narrow"
               + Environment.NewLine + "Voice control: Yes" + Environment.NewLine + "Time Lapse video: Yes" + Environment.NewLine +
               "Slo - Mo: 8x" + Environment.NewLine + "Live Streaming: 1080p" + Environment.NewLine +
               "GPS: Yes" + Environment.NewLine + "Microphones: 3" + Environment.NewLine +
               "Connectivity: Wi - Fi, Bluetooth" + Environment.NewLine + "Hero 8 Mods: Yes";

        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = productsLabel.Font;
            fontDialog1.ShowDialog();
            productsLabel.Font = fontDialog1.Font;
        }

        private void colourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = this.ForeColor;
            colorDialog1.ShowDialog();
            this.ForeColor = colorDialog1.Color;
        }

        private void aoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string aboutString = "Programmed by Lee Jia Yin, Meredithe Wan Jing Wen" +
                ", Tan Si Lei Jobelle, Zelde Choo Ye Yi (TI03 Group3)";
            string captionString = "About GoPro Products";
            MessageBox.Show(aboutString, captionString);
        }

        private void hero7SilverRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cameraPriceDecimal = 285.0m;
            cameraName = "Hero 7 Silver";

            priceTextBox.Text = cameraPriceDecimal.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Hero_7_Silver;

            addOnsGroupBox.Enabled = true;
            maintenancePackageGroupBox.Enabled = true;
            promotionCodeTextBox.Enabled = true;
            addToOrderButton.Enabled = true;

            specificationRichTextBox.Text = "Weight: 94g" + Environment.NewLine + "Dimensions: 62.3 W x 44.9 H x 28.3 D (mm)"
               + Environment.NewLine + "Photo: 10MP + WDR" + Environment.NewLine + "Video: 4K30" + Environment.NewLine +
               "Battery: Built - In" + Environment.NewLine + "Waterproof: 10m " + Environment.NewLine +
               "Touchscreen: Yes" + Environment.NewLine + "Stabilisation: Standard"
               + Environment.NewLine + "Digital lenses: Wide" + Environment.NewLine + "Voice control: Yes" + Environment.NewLine +
               "Time Lapse video: Yes" + Environment.NewLine + "Slo-Mo: 2x" + Environment.NewLine +
               "Live Streaming: No" + Environment.NewLine + "GPS: Yes" + Environment.NewLine +
               "Microphones: 2" + Environment.NewLine + "Connectivity: Wi-Fi, Bluetooth";
        }

        private void hero6BlackRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cameraPriceDecimal = 428.0m;
            cameraName = "Hero 6 Black";

            priceTextBox.Text = cameraPriceDecimal.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Hero_6_Black;

            addOnsGroupBox.Enabled = true;
            maintenancePackageGroupBox.Enabled = true;
            promotionCodeTextBox.Enabled = true;
            addToOrderButton.Enabled = true;


            specificationRichTextBox.Text = "Weight:117g" + Environment.NewLine + "Dimensions: 62 W x 45 H x 33 D(mm)"
                + Environment.NewLine + "Liveburst:No" + Environment.NewLine + "Video4k30" + Environment.NewLine
                + "Battery:Removable:" + Environment.NewLine + "Waterproof:10m" + Environment.NewLine + "Touchscreen:Yes"
                + Environment.NewLine + "Voice Control: Yes" + Environment.NewLine + "Time Lapse Video: Yes"
                + Environment.NewLine + "Slo-Mo 2x" + Environment.NewLine + "Live Streaming:No" + Environment.NewLine
                + "GPS:Yes" + Environment.NewLine + "Microphones:2" + Environment.NewLine + "Connectivity:Wifi, Bluetooth";

        }

        private void orderListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (orderListBox.SelectedIndex >=0)
            {
                deleteSelectedItemButton.Enabled = true;
            }
            else
            {
                deleteSelectedItemButton.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timeLabel.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }


        private void addToOrderButton_Click(object sender, EventArgs e)
        {
            int model = 0;
            string model_name = "";
            decimal model_cost = 0;
            bool threeway = false;
            decimal threeway_cost = 0;
            bool casey = false;
            decimal casey_cost = 0;
            bool sdcard = false;
            decimal sdcard_cost = 0;
            int maintenance = 0;
            decimal maintenance_cost = 0;
            decimal price = 0;


            Order newOrder = new Order(model,
                                model_name,
                                model_cost,
                                threeway,
                                threeway_cost,
                                casey,
                                casey_cost,
                                sdcard,
                                sdcard_cost,
                                maintenance,
                                maintenance_cost,
                                price);


            if (invisibleRadioButton.Checked)
            {
                MessageBox.Show("Please select a Camera!", "Error");
                orderListBox.Items.Clear();
                cancelOrderButton.Enabled = false;
                orderCompleteButton.Enabled = false;
            }
            else if (hero8BlackRadioButton.Checked)
            {
                newOrder.Model = 1;
                newOrder.Model_name = cameraName;
                newOrder.Model_cost = cameraPriceDecimal;
            }
            else if (hero7BlackRadioButton.Checked)
            {
                newOrder.Model = 2;
                newOrder.Model_name = cameraName;
                newOrder.Model_cost = cameraPriceDecimal;

            }
            else if (hero6BlackRadioButton.Checked)
            {
                newOrder.Model = 3;
                newOrder.Model_name = cameraName;
                newOrder.Model_cost = cameraPriceDecimal;
            }
            else if (hero5BlackRadioButton.Checked)
            {
                newOrder.Model = 4;
                newOrder.Model_name = cameraName;
                newOrder.Model_cost = cameraPriceDecimal;
            }
            else if (hero7WhiteRadioButton.Checked)
            {
                newOrder.Model = 5;
                newOrder.Model_name = cameraName;
                newOrder.Model_cost = cameraPriceDecimal;
            }
            else if (hero7SilverRadioButton.Checked)
            {
                newOrder.Model = 6;
                newOrder.Model_name = cameraName;
                newOrder.Model_cost = cameraPriceDecimal;
            }

            orderCompleteButton.Enabled = true;
            orderCompleteToolStripMenuItem.Enabled = true;
            cancelOrderButton.Enabled = true;


            if (threeWayCheckBox.Checked)
            {
                newOrder.ThreeWay = true;
                newOrder.ThreeWay_cost = addOnsPriceDecimal1;
            }

            if (caseyCheckBox.Checked)
            {
                newOrder.Casey = true;
                newOrder.Casey_cost = addOnsPriceDecimal1;
            }

            if (sdCardCheckBox.Checked)
            {
                newOrder.SDCard = true;
                newOrder.SDCard_cost = addOnsPriceDecimal1;
            }

            if (basicPackageRadioButton.Checked)
            {
                newOrder.Maintenance = 1;
                newOrder.Maintenance_cost = maintenancePriceDecimal;
            }
            if (advancedPackageRadioButton.Checked)
            {
                newOrder.Maintenance = 2;
                newOrder.Maintenance_cost = maintenancePriceDecimal;

            }
            if (premiumPackageRadioButton.Checked)
            {
                newOrder.Maintenance = 3;
                newOrder.Maintenance_cost = maintenancePriceDecimal;
            }
            if (noSelectionRadioButton.Checked)
            {
                newOrder.Maintenance = 0;
            }

            orderList.Add(newOrder);

            update_listbox();

            MessageBox.Show("Your order has been added", "Order Added");
            orderListBox.Items.Add(" ");
            clearForNextItem();

            cancelOrderButton.Enabled = true;
            promotionCodeTextBox.Enabled = true;

        }
        private void update_listbox()
        {
            orderListBox.Items.Clear();
            for (int i = 0; i < orderList.Count; i++)
            {
                orderList[i].Price = 0;
                orderListBox.Items.Add("Item: " + (i + 1).ToString("00") + ". " + orderList[i].Model_name + " " + orderList[i].Model_cost.ToString("C"));
                orderList[i].Price += orderList[i].Model_cost;

                if (orderList[i].ThreeWay == true)
                {
                    orderListBox.Items.Add("option a for " + (i + 1).ToString("00") + ". " + addOnsName1 + " " + orderList[i].ThreeWay_cost.ToString("C"));
                    orderList[i].Price += orderList[i].ThreeWay_cost;
                }

                if (orderList[i].Casey == true)
                {
                    orderListBox.Items.Add("option b for " + (i + 1).ToString("00") + ". " + addOnsName2 + " " + orderList[i].Casey_cost.ToString("C"));
                    orderList[i].Price += orderList[i].Casey_cost;
                }

                if (orderList[i].SDCard == true)
                {
                    orderListBox.Items.Add("option c for " + (i + 1).ToString("00") + ". " + addOnsName3 + " " + orderList[i].SDCard_cost.ToString("C"));
                    orderList[i].Price += orderList[i].SDCard_cost;
                }

                if (orderList[i].Maintenance > 0)
                {
                    orderListBox.Items.Add("option d for " + (i + 1).ToString("00") + ". " + maintenanceName + " " + orderList[i].Maintenance_cost.ToString("C"));
                    orderList[i].Price += orderList[i].Maintenance_cost;

                }
                orderListBox.Items.Add(" ");
            }

            orderListBox.Update();

        }


        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string aboutString = "Programmed by Lee Jia Yin, Meredithe Wan Jing Wen," +
                " Tan Si Lei Jobelle, Zelde Choo Ye Yi (TI03 Group3)";
            string captionString = "About GoPro Products - Orders";
            MessageBox.Show(aboutString, captionString);
        }

        private void summaryButton_Click(object sender, EventArgs e)
        {
            summaryForm smryForm;
            decimal subtotalDecimal = 0.0m;
            for (int i = 0; i < orderList.Count; i++)
            {
                subtotalDecimal += orderList[i].Price;
            }
            smryForm = new summaryForm(subtotalDecimal, currentDiscPrice);
            smryForm.ShowDialog();
            addToOrderButton.Enabled = false;
            orderCompleteButton.Enabled = false;
            orderCompleteToolStripMenuItem.Enabled = false;
        }

        private void GoProForm_Load(object sender, EventArgs e)
        {
            timer1.Start();
            dateLabel.Text = DateTime.Now.ToLongDateString();
            timeLabel.Text = DateTime.Now.ToLongTimeString();
        }

        private void orderCompleteButton_Click(object sender, EventArgs e)
        {
            decimal decDiscount = 0.0m;

            decimal subtotalDecimal = 0.0m;
            for (int i = 0; i < orderList.Count; i++)
            {
                subtotalDecimal += orderList[i].Price;
            }

            if (subtotalDecimal >= 680)
            {
                decDiscount += 0.25m;
            }
            else if (subtotalDecimal >= 550 && subtotalDecimal < 680)
            {
                decDiscount += 0.15m;
            }
            else if (subtotalDecimal >= 400 && subtotalDecimal < 550)
            {
                decDiscount += 0.10m;
            }
            decimal discountOnlyDecimal = decDiscount * subtotalDecimal;
            if (discountOnlyDecimal > 400)
            {
                discountOnlyDecimal = 400.0m;
                MessageBox.Show("Discount is capped at $300.", "Notice");
            }


            if (promotionCodeTextBox.Text == ("STAYHOME10"))
            {
                promoDecimal += 10.0m;
                MessageBox.Show("Promotion code applied.");
            }
            if (promotionCodeTextBox.Text == ("SPECIAL20"))
            {
                if (subtotalDecimal >= 500)
                {
                    promoDecimal += 20.0m;
                    MessageBox.Show("Promotion code applied.");
                }
                else
                {
                    MessageBox.Show("Minimum $500 spending is needed", "Error");
                }
            }
            else if (promotionCodeTextBox.Text == ("NEWUSER30"))
            {
                promoDecimal += 30.0m;
                MessageBox.Show("Promotion code applied.");
            }
            currentDiscPrice = discountOnlyDecimal + promoDecimal;
            totalAfterDiscountDecimal = subtotalDecimal - currentDiscPrice;
            string dueString = "Amount Due: " + totalAfterDiscountDecimal.ToString("C");
                        
            MessageBox.Show(dueString, "Order Complete");
            clearForNextItem();
            orderListBox.Items.Clear();
            itemInteger = 0;
            orderInteger--;
            totalAddOnsPriceDecimal = 0m;
            totalCameraPriceDecimal = 0m;
            totalMaintenancePriceDecimal = 0m;

            orderInteger++;
            totalSalesDecimal += totalAfterDiscountDecimal;

            summaryButton.Enabled = true;
            checkOutButton.Enabled = true;
            deleteSelectedItemButton.Enabled = false;
            cancelOrderButton.Enabled = false;
            orderCompleteButton.Enabled = false;
            orderCompleteToolStripMenuItem.Enabled = false;
            orderListBox.Items.Clear();
            totalAfterDiscountDecimal = 0m;

        }

        private void imageVisibleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            productPictureBox.Visible = imageVisibleCheckBox.Checked;
        }

        private void invisibleRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            addOnsGroupBox.Enabled = false;
            maintenancePackageGroupBox.Enabled = false;
            promotionCodeTextBox.Enabled = false;
            addToOrderButton.Enabled = false;

        }

        private void cancelOrderButton_Click(object sender, EventArgs e)
        {
            string messageString = "Confirm Cancel Order?";
            DialogResult userResponseDialogResult;

            userResponseDialogResult = MessageBox.Show(messageString, "Cancel Order",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (userResponseDialogResult == DialogResult.Yes)
            {
                clearForNextItem();
                orderListBox.Items.Clear();
                itemInteger = 0;
                orderInteger--;
                totalAddOnsPriceDecimal = 0m;
                totalCameraPriceDecimal = 0m;
                totalMaintenancePriceDecimal = 0m;
            }
            cancelOrderButton.Enabled = false;
            deleteSelectedItemButton.Enabled = false;
            orderCompleteButton.Enabled = false;
            orderCompleteToolStripMenuItem.Enabled = false;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            string messageString = "Are you sure you want to exit?";
            DialogResult userResponseDialogResult;

            userResponseDialogResult = MessageBox.Show(messageString, "Exit",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (userResponseDialogResult == DialogResult.Yes)
            {
                this.Close();
            }

        }

        private void checkOutButton_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("Payment successful. Thank You!", "Check Out");
            decimal tempTotal = 0;
            for (int i = 0; i < orderList.Count; i++)
            {
                goproArray[orderList[i].Model - 1] += 1;
                if (orderList[i].ThreeWay) {
                    addOnArray[0] += 1;
                }
                if (orderList[i].Casey)
                {
                    addOnArray[1] += 1;
                }
                if (orderList[i].SDCard)
                {
                    addOnArray[2] += 1;
                }
                tempTotal += orderList[i].Price;
            }
            tempTotal = tempTotal - currentDiscPrice;
            transactionList.Add(tempTotal);

            summaryButton.Enabled = false;
            checkOutButton.Enabled = false;
            orderList.Clear();
            totalAfterDiscountDecimal = 0;
            totalAddOnsPriceDecimal = 0;
        }

        private void businessReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BusinessReport businessreportForm;
            decimal totalRevenue = 0.0m;
            int item_count = 0;
            int order_count = transactionList.Count;
            for (int i = 0; i < transactionList.Count; i++)
            {
                totalRevenue += transactionList[i];
            }
            int camera_iter = 0;
            int addon_iter = 0;
            int maintenance_iter = 0;
            int camera_count = 0;
            int addon_count = 0;
            int maintenance_count = 0;
            

            for (int i = 0; i < goproArray.Length; i++)
            {
                item_count += goproArray[i];
                if (camera_count < goproArray[i])
                {
                    camera_count = goproArray[i];
                    camera_iter = i;
                }
            }

            for (int i = 0; i < addOnArray.Length; i++)
            {
                if (addon_count < addOnArray[i])
                {
                    addon_count = addOnArray[i];
                    addon_iter = i;
                }
            }

            for (int i = 0; i < maintenanceArray.Length; i++)
            {
                if (addon_count < addOnArray[i])
                {
                    addon_count = addOnArray[i];
                    addon_iter = i;
                }
            }

            string bestCamera = "";
            string bestAddOn = "";
            string bestMaintenance = "";

            switch (camera_iter)
            {
                case 0:
                    bestCamera = "Hero 8 Black";
                    break;

                case 1:
                    bestCamera = "Hero 7 Black";
                    break;

                case 2:
                    bestCamera = "Hero 6 Black";
                    break;

                case 3:
                    bestCamera = "Hero 5 Black";
                    break;

                case 4:
                    bestCamera = "Hero 7 White";
                    break;

                case 5:
                    bestCamera = "Hero 7 Silver";
                    break;

            }

            switch (addon_iter)
            {
                case 0:
                    bestAddOn = "GoPro 3-Way";
                    break;

                case 1:
                    bestAddOn = "GoPro Casey";
                    break;

                case 2:
                    bestAddOn = "SD Card";
                    break;

            }

            switch(maintenance_iter)
            {
                case 0:
                    bestMaintenance = "Basic Package";
                break;

                case 1:
                    bestMaintenance = "Advanced Package";
                break;

                case 2:
                    bestMaintenance = "Premium Package";
                break;

            }

            //businessreportForm = new BusinessReport(item_count, order_count, totalRevenue, top_camera, top_addon);
            businessreportForm = new BusinessReport(item_count, order_count, maintenance_count, totalRevenue, bestCamera, bestAddOn, bestMaintenance);
            businessreportForm.ShowDialog();
        }

        private void deleteSelectedItemButton_Click(object sender, EventArgs e)
        {
            string messageString = "Are you sure to delete selected item?";

            DialogResult userResponseDialogResult;

            userResponseDialogResult = MessageBox.Show(messageString, "Delete Item",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (userResponseDialogResult == DialogResult.Yes)
            {
                string strCamera = orderListBox.SelectedItem.ToString();
                if(strCamera.Substring(0,6).Equals("option"))
                {
                    int camera_order_index = int.Parse(strCamera.Substring(13, 2)) - 1;
                    string choice = strCamera.Substring(7, 1);

                    switch(choice)
                    {
                        case "a":
                            orderList[camera_order_index].ThreeWay = false;
                            break;
                        case "b":
                            orderList[camera_order_index].Casey = false;
                            break;
                        case "c":
                            orderList[camera_order_index].SDCard = false;
                            break;
                        case "d":
                            orderList[camera_order_index].Maintenance = 0;
                            break;

                    }
                }
                else if(strCamera.Substring(0, 6).Equals("Item: "))
                {
                    int index = int.Parse(strCamera.Substring(6, 2)) - 1;
                    orderList.RemoveAt(index);
                }
            }
            update_listbox();
        }
        
        private void hero7BlackRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cameraPriceDecimal = 495.0m;
            cameraName = "Hero 7 Black";

            priceTextBox.Text = cameraPriceDecimal.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Hero_7_Black;

            addOnsGroupBox.Enabled = true;
            maintenancePackageGroupBox.Enabled = true;
            promotionCodeTextBox.Enabled = true;
            addToOrderButton.Enabled = true;


            specificationRichTextBox.Text = "Weight: 116g" + Environment.NewLine + "Dimensions: 62.3 W x 44.9 H x 33 D(mm)"
               + Environment.NewLine + "Photo: 12MP + SuperPhoto with HDR" + Environment.NewLine + "LiveBurst: No"
               + Environment.NewLine + "Video: 4K60" + Environment.NewLine + "Battery: Removable"
               + Environment.NewLine + "Waterproof: 10m" + Environment.NewLine + "Touchscreen: Yes" + Environment.NewLine
               + "Stabilisation: HyperSmooth" + Environment.NewLine + "Digital lenses: SuperView, Wide, Linear"
               + Environment.NewLine + "Voice control: Yes" + Environment.NewLine + "Time Lapse video: Yes" + Environment.NewLine + "Slo-Mo: 8x"
               + Environment.NewLine + "Live Streaming: 720p" + Environment.NewLine + "GPS: Yes" + Environment.NewLine + "Microphones: 3"
               + Environment.NewLine + "Connectivity: Wi - Fi, Bluetooth";

        }

        private void hero7WhiteRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cameraPriceDecimal = 275.0m;
            cameraName = "Hero 7 White";

            priceTextBox.Text = cameraPriceDecimal.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Hero_7_White;

            addOnsGroupBox.Enabled = true;
            maintenancePackageGroupBox.Enabled = true;
            promotionCodeTextBox.Enabled = true;
            addToOrderButton.Enabled = true;


            specificationRichTextBox.Text = "Weight: 92g" + Environment.NewLine + "Dimensions: 62.3 W x 44.9 H x 28.3 D (mm)" + Environment.NewLine + "Photo: 10MP + WDR" + Environment.NewLine + "LiveBurst: No" + Environment.NewLine +
           "Video: 1440p60" + Environment.NewLine + "Battery: Built-In" + Environment.NewLine +
            "Waterproof: 10m" + Environment.NewLine + "Touchscreen: Yes" + Environment.NewLine +
            "Stabilisation: Standard" + Environment.NewLine + "Voice control: Yes" + Environment.NewLine + "Time Lapse video: Yes" + Environment.NewLine + "Slo-Mo: 2x" + Environment.NewLine + "Live Streaming: No" + Environment.NewLine +
            "GPS: Yes" + Environment.NewLine + "Microphones: 2" + Environment.NewLine +
            "Connectivity: Wi-Fi, Bluetooth";

        }

        private void hero5BlackRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            cameraPriceDecimal = 250.0m;
            cameraName = "Hero 5 Black";

            priceTextBox.Text = cameraPriceDecimal.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Hero_5_Black;

            addOnsGroupBox.Enabled = true;
            maintenancePackageGroupBox.Enabled = true;
            promotionCodeTextBox.Enabled = true;
            addToOrderButton.Enabled = true;


            specificationRichTextBox.Text = "Weight: 118g" + Environment.NewLine + "Dimensions: 62 W x 45 H x 33 D (mm)"
               + Environment.NewLine + "Photo: 12MP" + Environment.NewLine + "LiveBurst: No" + Environment.NewLine
               + "Video: 1080p120" + Environment.NewLine + "Battery: Removable" + Environment.NewLine + "Waterproof: 10m"
               + Environment.NewLine + "Touchscreen: Yes" + Environment.NewLine + "Voice Control: Yes" + Environment.NewLine
               + "Time Lapse Video: Yes" + Environment.NewLine + "Slo-Mo: 2x" + Environment.NewLine + "Live Streaming: No"
               + Environment.NewLine + "GPS: Yes" + Environment.NewLine + "Microphones: 2:" + Environment.NewLine + "Connectivity: Wifi, Bluetooth";

        }

        private void threeWayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            addOnsPriceDecimal1 = 115.0m;
            addOnsName1 = "ThreeWay";

            priceTextBox.Text = addOnsPriceDecimal1.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Threeway;

            specificationRichTextBox.Text = "A tripod, an extension arm and a grip all in one" + Environment.NewLine
                + "Waterproof construction for use in and out of the water" + Environment.NewLine
                + "Dimensions: 50.8cm when fully extended and collapses to 19cm" + Environment.NewLine + "Compatible with all";

        }

        private void caseyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            addOnsPriceDecimal2 = 82.0m;
            addOnsName2 = "Casey";

            priceTextBox.Text = addOnsPriceDecimal2.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.Casey;

            specificationRichTextBox.Text = "Weight: 286g" + Environment.NewLine + "Dimensions: 165 W x 89 H x 221 D(mm)"
               + Environment.NewLine + "Capacity: 2 or more cameras, mounts and accessories" + Environment.NewLine
               + "Fully customizable" + Environment.NewLine + "Durable, semi - rigid shell and padded inner walls" + Environment.NewLine
               + "Weather - resistant design guards against light rain and snow" + Environment.NewLine
               + "Removable pouch for batteries, thumb screws, micro SD cards etc." + Environment.NewLine + "Compatible with all";

        }

        private void sdCardCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            addOnsPriceDecimal3 = 30.0m;
            addOnsName3 = "SD Card";

            priceTextBox.Text = addOnsPriceDecimal3.ToString("C");
            productPictureBox.Image = Proj2_TI03_MereditheWan.Properties.Resources.SD_Card;

            specificationRichTextBox.Text = "MicroSD (with SD adapter)" + Environment.NewLine +  "128GB capacity" 
                +Environment.NewLine + "Waterproof" +Environment.NewLine + "4K ready" +Environment.NewLine 
                + "Compatible with all";

        }

        private void basicPackageRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            maintenancePriceDecimal = 80.0m;
            maintenanceName = "Basic Package";

            priceTextBox.Text = maintenancePriceDecimal.ToString("C");
            productPictureBox.Image = null;
            specificationRichTextBox.Text = "6-month warranty";
        }

        private void advancedPackageRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            maintenancePriceDecimal = 200.0m;
            maintenanceName = "Advanced Package";

            priceTextBox.Text = maintenancePriceDecimal.ToString("C");
            productPictureBox.Image = null;
            specificationRichTextBox.Text = "2 years warranty";
        }

        private void premiumPackageRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            maintenancePriceDecimal = 350.0m;
            maintenanceName = "Premium Package";

            priceTextBox.Text = maintenancePriceDecimal.ToString("C");
            productPictureBox.Image = null;
            specificationRichTextBox.Text = "5 years warranty";
        }

        private void priceTextBox_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
