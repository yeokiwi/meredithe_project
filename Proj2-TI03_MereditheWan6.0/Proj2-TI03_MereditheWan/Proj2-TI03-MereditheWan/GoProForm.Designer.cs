﻿namespace Proj2_TI03_MereditheWan
{
    partial class GoProForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.hero7WhiteRadioButton = new System.Windows.Forms.RadioButton();
            this.noSelectionRadioButton = new System.Windows.Forms.RadioButton();
            this.sdCardCheckBox = new System.Windows.Forms.CheckBox();
            this.caseyCheckBox = new System.Windows.Forms.CheckBox();
            this.threeWayCheckBox = new System.Windows.Forms.CheckBox();
            this.invisibleRadioButton = new System.Windows.Forms.RadioButton();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.businessReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedItemButton = new System.Windows.Forms.Button();
            this.cancelOrderButton = new System.Windows.Forms.Button();
            this.summaryButton = new System.Windows.Forms.Button();
            this.orderCompleteButton = new System.Windows.Forms.Button();
            this.productInformationGroupBox = new System.Windows.Forms.GroupBox();
            this.specificationRichTextBox = new System.Windows.Forms.RichTextBox();
            this.productImageLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.specificationLabel = new System.Windows.Forms.Label();
            this.productPictureBox = new System.Windows.Forms.PictureBox();
            this.imageVisibleCheckBox = new System.Windows.Forms.CheckBox();
            this.maintenancePackageGroupBox = new System.Windows.Forms.GroupBox();
            this.premiumPackageRadioButton = new System.Windows.Forms.RadioButton();
            this.advancedPackageRadioButton = new System.Windows.Forms.RadioButton();
            this.basicPackageRadioButton = new System.Windows.Forms.RadioButton();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addOnsGroupBox = new System.Windows.Forms.GroupBox();
            this.colourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productSpecificationLabel = new System.Windows.Forms.Label();
            this.goproGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.hero7SilverRadioButton = new System.Windows.Forms.RadioButton();
            this.hero5BlackRadioButton = new System.Windows.Forms.RadioButton();
            this.hero6BlackRadioButton = new System.Windows.Forms.RadioButton();
            this.hero7BlackRadioButton = new System.Windows.Forms.RadioButton();
            this.hero8BlackRadioButton = new System.Windows.Forms.RadioButton();
            this.orderCompleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.promotionLabel = new System.Windows.Forms.Label();
            this.bestsellerGroupBox = new System.Windows.Forms.GroupBox();
            this.hero6BlackLabel = new System.Windows.Forms.Label();
            this.hero7BlackLabel = new System.Windows.Forms.Label();
            this.hero8BlackLabel = new System.Windows.Forms.Label();
            this.hero6BlackPictureBox = new System.Windows.Forms.PictureBox();
            this.hero7BlackPictureBox = new System.Windows.Forms.PictureBox();
            this.hero8BlackPictureBox = new System.Windows.Forms.PictureBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.promotionCodeLabel = new System.Windows.Forms.Label();
            this.addToOrderButton = new System.Windows.Forms.Button();
            this.checkOutButton = new System.Windows.Forms.Button();
            this.promotionCodeTextBox = new System.Windows.Forms.TextBox();
            this.orderSummaryGroupBox = new System.Windows.Forms.GroupBox();
            this.orderListBox = new System.Windows.Forms.ListBox();
            this.productsLabel = new System.Windows.Forms.Label();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.dateLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.discountPackagesGroupBox = new System.Windows.Forms.GroupBox();
            this.dpCapLabel = new System.Windows.Forms.Label();
            this.dp3Label = new System.Windows.Forms.Label();
            this.dp2Label = new System.Windows.Forms.Label();
            this.dp1Label = new System.Windows.Forms.Label();
            this.pc1Label = new System.Windows.Forms.Label();
            this.dc2Label = new System.Windows.Forms.Label();
            this.pc3Label = new System.Windows.Forms.Label();
            this.promotionCodesGroupBox = new System.Windows.Forms.GroupBox();
            this.productInformationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productPictureBox)).BeginInit();
            this.maintenancePackageGroupBox.SuspendLayout();
            this.addOnsGroupBox.SuspendLayout();
            this.goproGroupBox.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.bestsellerGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hero6BlackPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hero7BlackPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hero8BlackPictureBox)).BeginInit();
            this.orderSummaryGroupBox.SuspendLayout();
            this.discountPackagesGroupBox.SuspendLayout();
            this.promotionCodesGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // hero7WhiteRadioButton
            // 
            this.hero7WhiteRadioButton.AutoSize = true;
            this.hero7WhiteRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero7WhiteRadioButton.Location = new System.Drawing.Point(296, 74);
            this.hero7WhiteRadioButton.Name = "hero7WhiteRadioButton";
            this.hero7WhiteRadioButton.Size = new System.Drawing.Size(212, 29);
            this.hero7WhiteRadioButton.TabIndex = 4;
            this.hero7WhiteRadioButton.Text = "GoPro Hero 7 &White";
            this.hero7WhiteRadioButton.UseVisualStyleBackColor = true;
            this.hero7WhiteRadioButton.CheckedChanged += new System.EventHandler(this.hero7WhiteRadioButton_CheckedChanged);
            // 
            // noSelectionRadioButton
            // 
            this.noSelectionRadioButton.AutoSize = true;
            this.noSelectionRadioButton.Checked = true;
            this.noSelectionRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noSelectionRadioButton.Location = new System.Drawing.Point(392, 51);
            this.noSelectionRadioButton.Name = "noSelectionRadioButton";
            this.noSelectionRadioButton.Size = new System.Drawing.Size(160, 29);
            this.noSelectionRadioButton.TabIndex = 10;
            this.noSelectionRadioButton.TabStop = true;
            this.noSelectionRadioButton.Text = "No Selection";
            this.noSelectionRadioButton.UseVisualStyleBackColor = true;
            // 
            // sdCardCheckBox
            // 
            this.sdCardCheckBox.AutoSize = true;
            this.sdCardCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdCardCheckBox.Location = new System.Drawing.Point(16, 115);
            this.sdCardCheckBox.Name = "sdCardCheckBox";
            this.sdCardCheckBox.Size = new System.Drawing.Size(364, 29);
            this.sdCardCheckBox.TabIndex = 9;
            this.sdCardCheckBox.TabStop = false;
            this.sdCardCheckBox.Text = "SanDisk &Extreme 128GB microSDXC";
            this.sdCardCheckBox.UseVisualStyleBackColor = true;
            this.sdCardCheckBox.CheckedChanged += new System.EventHandler(this.sdCardCheckBox_CheckedChanged);
            // 
            // caseyCheckBox
            // 
            this.caseyCheckBox.AutoSize = true;
            this.caseyCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.caseyCheckBox.Location = new System.Drawing.Point(16, 75);
            this.caseyCheckBox.Name = "caseyCheckBox";
            this.caseyCheckBox.Size = new System.Drawing.Size(156, 29);
            this.caseyCheckBox.TabIndex = 8;
            this.caseyCheckBox.TabStop = false;
            this.caseyCheckBox.Text = "GoPro &Casey";
            this.caseyCheckBox.UseVisualStyleBackColor = true;
            this.caseyCheckBox.CheckedChanged += new System.EventHandler(this.caseyCheckBox_CheckedChanged);
            // 
            // threeWayCheckBox
            // 
            this.threeWayCheckBox.AutoSize = true;
            this.threeWayCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeWayCheckBox.Location = new System.Drawing.Point(16, 35);
            this.threeWayCheckBox.Name = "threeWayCheckBox";
            this.threeWayCheckBox.Size = new System.Drawing.Size(158, 29);
            this.threeWayCheckBox.TabIndex = 7;
            this.threeWayCheckBox.TabStop = false;
            this.threeWayCheckBox.Text = "GoPro &3-Way";
            this.threeWayCheckBox.UseVisualStyleBackColor = true;
            this.threeWayCheckBox.CheckedChanged += new System.EventHandler(this.threeWayCheckBox_CheckedChanged);
            // 
            // invisibleRadioButton
            // 
            this.invisibleRadioButton.AutoSize = true;
            this.invisibleRadioButton.Checked = true;
            this.invisibleRadioButton.Location = new System.Drawing.Point(413, 153);
            this.invisibleRadioButton.Name = "invisibleRadioButton";
            this.invisibleRadioButton.Size = new System.Drawing.Size(116, 29);
            this.invisibleRadioButton.TabIndex = 7;
            this.invisibleRadioButton.TabStop = true;
            this.invisibleRadioButton.Text = "Invisible";
            this.invisibleRadioButton.UseVisualStyleBackColor = true;
            this.invisibleRadioButton.Visible = false;
            this.invisibleRadioButton.CheckedChanged += new System.EventHandler(this.invisibleRadioButton_CheckedChanged);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.businessReportToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(54, 29);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // businessReportToolStripMenuItem
            // 
            this.businessReportToolStripMenuItem.Name = "businessReportToolStripMenuItem";
            this.businessReportToolStripMenuItem.Size = new System.Drawing.Size(239, 34);
            this.businessReportToolStripMenuItem.Text = "&Business Report";
            this.businessReportToolStripMenuItem.Click += new System.EventHandler(this.businessReportToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(239, 34);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // deleteSelectedItemButton
            // 
            this.deleteSelectedItemButton.Enabled = false;
            this.deleteSelectedItemButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedItemButton.Location = new System.Drawing.Point(1684, 477);
            this.deleteSelectedItemButton.Name = "deleteSelectedItemButton";
            this.deleteSelectedItemButton.Size = new System.Drawing.Size(199, 66);
            this.deleteSelectedItemButton.TabIndex = 56;
            this.deleteSelectedItemButton.Text = "&Delete Selected Item";
            this.deleteSelectedItemButton.UseVisualStyleBackColor = true;
            this.deleteSelectedItemButton.Click += new System.EventHandler(this.deleteSelectedItemButton_Click);
            // 
            // cancelOrderButton
            // 
            this.cancelOrderButton.Enabled = false;
            this.cancelOrderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelOrderButton.Location = new System.Drawing.Point(1684, 560);
            this.cancelOrderButton.Name = "cancelOrderButton";
            this.cancelOrderButton.Size = new System.Drawing.Size(199, 55);
            this.cancelOrderButton.TabIndex = 45;
            this.cancelOrderButton.Text = "Cancel &Order";
            this.cancelOrderButton.UseVisualStyleBackColor = true;
            this.cancelOrderButton.Click += new System.EventHandler(this.cancelOrderButton_Click);
            // 
            // summaryButton
            // 
            this.summaryButton.Enabled = false;
            this.summaryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.summaryButton.Location = new System.Drawing.Point(1684, 270);
            this.summaryButton.Name = "summaryButton";
            this.summaryButton.Size = new System.Drawing.Size(199, 58);
            this.summaryButton.TabIndex = 3;
            this.summaryButton.Text = "Su&mmary";
            this.summaryButton.UseVisualStyleBackColor = true;
            this.summaryButton.Click += new System.EventHandler(this.summaryButton_Click);
            // 
            // orderCompleteButton
            // 
            this.orderCompleteButton.Enabled = false;
            this.orderCompleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderCompleteButton.Location = new System.Drawing.Point(1684, 188);
            this.orderCompleteButton.Name = "orderCompleteButton";
            this.orderCompleteButton.Size = new System.Drawing.Size(199, 61);
            this.orderCompleteButton.TabIndex = 2;
            this.orderCompleteButton.Text = "Order Com&plete";
            this.orderCompleteButton.UseVisualStyleBackColor = true;
            this.orderCompleteButton.Click += new System.EventHandler(this.orderCompleteButton_Click);
            // 
            // productInformationGroupBox
            // 
            this.productInformationGroupBox.Controls.Add(this.specificationRichTextBox);
            this.productInformationGroupBox.Controls.Add(this.productImageLabel);
            this.productInformationGroupBox.Controls.Add(this.priceLabel);
            this.productInformationGroupBox.Controls.Add(this.priceTextBox);
            this.productInformationGroupBox.Controls.Add(this.specificationLabel);
            this.productInformationGroupBox.Controls.Add(this.productPictureBox);
            this.productInformationGroupBox.Controls.Add(this.imageVisibleCheckBox);
            this.productInformationGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productInformationGroupBox.Location = new System.Drawing.Point(634, 75);
            this.productInformationGroupBox.Name = "productInformationGroupBox";
            this.productInformationGroupBox.Size = new System.Drawing.Size(484, 782);
            this.productInformationGroupBox.TabIndex = 47;
            this.productInformationGroupBox.TabStop = false;
            this.productInformationGroupBox.Text = "Product Information";
            // 
            // specificationRichTextBox
            // 
            this.specificationRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specificationRichTextBox.Location = new System.Drawing.Point(208, 91);
            this.specificationRichTextBox.Name = "specificationRichTextBox";
            this.specificationRichTextBox.ReadOnly = true;
            this.specificationRichTextBox.Size = new System.Drawing.Size(268, 449);
            this.specificationRichTextBox.TabIndex = 11;
            this.specificationRichTextBox.TabStop = false;
            this.specificationRichTextBox.Text = "";
            // 
            // productImageLabel
            // 
            this.productImageLabel.AutoSize = true;
            this.productImageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productImageLabel.Location = new System.Drawing.Point(49, 645);
            this.productImageLabel.Name = "productImageLabel";
            this.productImageLabel.Size = new System.Drawing.Size(138, 25);
            this.productImageLabel.TabIndex = 10;
            this.productImageLabel.Text = "Product Image";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(69, 47);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(118, 25);
            this.priceLabel.TabIndex = 8;
            this.priceLabel.Text = "Price (SGD)";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceTextBox.Location = new System.Drawing.Point(208, 42);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.ReadOnly = true;
            this.priceTextBox.Size = new System.Drawing.Size(128, 30);
            this.priceTextBox.TabIndex = 9;
            this.priceTextBox.TabStop = false;
            this.priceTextBox.TextChanged += new System.EventHandler(this.priceTextBox_TextChanged);
            // 
            // specificationLabel
            // 
            this.specificationLabel.AutoSize = true;
            this.specificationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specificationLabel.Location = new System.Drawing.Point(10, 308);
            this.specificationLabel.Name = "specificationLabel";
            this.specificationLabel.Size = new System.Drawing.Size(177, 25);
            this.specificationLabel.TabIndex = 7;
            this.specificationLabel.Text = "Detail Specification";
            // 
            // productPictureBox
            // 
            this.productPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.productPictureBox.ErrorImage = null;
            this.productPictureBox.Location = new System.Drawing.Point(208, 562);
            this.productPictureBox.Name = "productPictureBox";
            this.productPictureBox.Size = new System.Drawing.Size(252, 197);
            this.productPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.productPictureBox.TabIndex = 3;
            this.productPictureBox.TabStop = false;
            // 
            // imageVisibleCheckBox
            // 
            this.imageVisibleCheckBox.AutoSize = true;
            this.imageVisibleCheckBox.Checked = true;
            this.imageVisibleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.imageVisibleCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imageVisibleCheckBox.Location = new System.Drawing.Point(32, 729);
            this.imageVisibleCheckBox.Name = "imageVisibleCheckBox";
            this.imageVisibleCheckBox.Size = new System.Drawing.Size(155, 29);
            this.imageVisibleCheckBox.TabIndex = 6;
            this.imageVisibleCheckBox.TabStop = false;
            this.imageVisibleCheckBox.Text = "Image &Visible";
            this.imageVisibleCheckBox.UseVisualStyleBackColor = true;
            this.imageVisibleCheckBox.CheckedChanged += new System.EventHandler(this.imageVisibleCheckBox_CheckedChanged);
            // 
            // maintenancePackageGroupBox
            // 
            this.maintenancePackageGroupBox.Controls.Add(this.noSelectionRadioButton);
            this.maintenancePackageGroupBox.Controls.Add(this.premiumPackageRadioButton);
            this.maintenancePackageGroupBox.Controls.Add(this.advancedPackageRadioButton);
            this.maintenancePackageGroupBox.Controls.Add(this.basicPackageRadioButton);
            this.maintenancePackageGroupBox.Enabled = false;
            this.maintenancePackageGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maintenancePackageGroupBox.Location = new System.Drawing.Point(12, 495);
            this.maintenancePackageGroupBox.Name = "maintenancePackageGroupBox";
            this.maintenancePackageGroupBox.Size = new System.Drawing.Size(578, 126);
            this.maintenancePackageGroupBox.TabIndex = 49;
            this.maintenancePackageGroupBox.TabStop = false;
            this.maintenancePackageGroupBox.Text = "Select Maintainance Package (Optional)";
            // 
            // premiumPackageRadioButton
            // 
            this.premiumPackageRadioButton.AutoSize = true;
            this.premiumPackageRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.premiumPackageRadioButton.Location = new System.Drawing.Point(264, 51);
            this.premiumPackageRadioButton.Name = "premiumPackageRadioButton";
            this.premiumPackageRadioButton.Size = new System.Drawing.Size(114, 29);
            this.premiumPackageRadioButton.TabIndex = 12;
            this.premiumPackageRadioButton.Text = "Premi&um";
            this.premiumPackageRadioButton.UseVisualStyleBackColor = true;
            this.premiumPackageRadioButton.CheckedChanged += new System.EventHandler(this.premiumPackageRadioButton_CheckedChanged);
            // 
            // advancedPackageRadioButton
            // 
            this.advancedPackageRadioButton.AutoSize = true;
            this.advancedPackageRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advancedPackageRadioButton.Location = new System.Drawing.Point(118, 51);
            this.advancedPackageRadioButton.Name = "advancedPackageRadioButton";
            this.advancedPackageRadioButton.Size = new System.Drawing.Size(126, 29);
            this.advancedPackageRadioButton.TabIndex = 11;
            this.advancedPackageRadioButton.Text = "A&dvanced";
            this.advancedPackageRadioButton.UseVisualStyleBackColor = true;
            this.advancedPackageRadioButton.CheckedChanged += new System.EventHandler(this.advancedPackageRadioButton_CheckedChanged);
            // 
            // basicPackageRadioButton
            // 
            this.basicPackageRadioButton.AutoSize = true;
            this.basicPackageRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basicPackageRadioButton.Location = new System.Drawing.Point(22, 51);
            this.basicPackageRadioButton.Name = "basicPackageRadioButton";
            this.basicPackageRadioButton.Size = new System.Drawing.Size(90, 29);
            this.basicPackageRadioButton.TabIndex = 10;
            this.basicPackageRadioButton.Text = "&Basic ";
            this.basicPackageRadioButton.UseVisualStyleBackColor = true;
            this.basicPackageRadioButton.CheckedChanged += new System.EventHandler(this.basicPackageRadioButton_CheckedChanged);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(65, 29);
            this.aboutToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(164, 34);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // addOnsGroupBox
            // 
            this.addOnsGroupBox.Controls.Add(this.sdCardCheckBox);
            this.addOnsGroupBox.Controls.Add(this.caseyCheckBox);
            this.addOnsGroupBox.Controls.Add(this.threeWayCheckBox);
            this.addOnsGroupBox.Enabled = false;
            this.addOnsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addOnsGroupBox.Location = new System.Drawing.Point(18, 306);
            this.addOnsGroupBox.Name = "addOnsGroupBox";
            this.addOnsGroupBox.Size = new System.Drawing.Size(482, 168);
            this.addOnsGroupBox.TabIndex = 43;
            this.addOnsGroupBox.TabStop = false;
            this.addOnsGroupBox.Text = "Select Add-Ons (Optional)";
            // 
            // colourToolStripMenuItem
            // 
            this.colourToolStripMenuItem.Name = "colourToolStripMenuItem";
            this.colourToolStripMenuItem.Size = new System.Drawing.Size(242, 34);
            this.colourToolStripMenuItem.Text = "&Colour...";
            this.colourToolStripMenuItem.Click += new System.EventHandler(this.colourToolStripMenuItem_Click);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(242, 34);
            this.fontToolStripMenuItem.Text = "&Font...";
            this.fontToolStripMenuItem.Click += new System.EventHandler(this.fontToolStripMenuItem_Click);
            // 
            // productSpecificationLabel
            // 
            this.productSpecificationLabel.AutoSize = true;
            this.productSpecificationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productSpecificationLabel.Location = new System.Drawing.Point(318, 325);
            this.productSpecificationLabel.Name = "productSpecificationLabel";
            this.productSpecificationLabel.Size = new System.Drawing.Size(0, 22);
            this.productSpecificationLabel.TabIndex = 39;
            // 
            // goproGroupBox
            // 
            this.goproGroupBox.Controls.Add(this.label1);
            this.goproGroupBox.Controls.Add(this.invisibleRadioButton);
            this.goproGroupBox.Controls.Add(this.hero7WhiteRadioButton);
            this.goproGroupBox.Controls.Add(this.hero7SilverRadioButton);
            this.goproGroupBox.Controls.Add(this.hero5BlackRadioButton);
            this.goproGroupBox.Controls.Add(this.hero6BlackRadioButton);
            this.goproGroupBox.Controls.Add(this.hero7BlackRadioButton);
            this.goproGroupBox.Controls.Add(this.hero8BlackRadioButton);
            this.goproGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goproGroupBox.Location = new System.Drawing.Point(12, 102);
            this.goproGroupBox.Name = "goproGroupBox";
            this.goproGroupBox.Size = new System.Drawing.Size(578, 198);
            this.goproGroupBox.TabIndex = 40;
            this.goproGroupBox.TabStop = false;
            this.goproGroupBox.Text = "Select Model";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "*Select a camera to add to order";
            // 
            // hero7SilverRadioButton
            // 
            this.hero7SilverRadioButton.AutoSize = true;
            this.hero7SilverRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero7SilverRadioButton.Location = new System.Drawing.Point(27, 74);
            this.hero7SilverRadioButton.Name = "hero7SilverRadioButton";
            this.hero7SilverRadioButton.Size = new System.Drawing.Size(210, 29);
            this.hero7SilverRadioButton.TabIndex = 3;
            this.hero7SilverRadioButton.Text = "GoPro Hero 7 &Silver";
            this.hero7SilverRadioButton.UseVisualStyleBackColor = true;
            this.hero7SilverRadioButton.CheckedChanged += new System.EventHandler(this.hero7SilverRadioButton_CheckedChanged);
            // 
            // hero5BlackRadioButton
            // 
            this.hero5BlackRadioButton.AutoSize = true;
            this.hero5BlackRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero5BlackRadioButton.Location = new System.Drawing.Point(296, 118);
            this.hero5BlackRadioButton.Name = "hero5BlackRadioButton";
            this.hero5BlackRadioButton.Size = new System.Drawing.Size(209, 29);
            this.hero5BlackRadioButton.TabIndex = 6;
            this.hero5BlackRadioButton.Text = "GoPro Hero &5 Black";
            this.hero5BlackRadioButton.UseVisualStyleBackColor = true;
            this.hero5BlackRadioButton.CheckedChanged += new System.EventHandler(this.hero5BlackRadioButton_CheckedChanged);
            // 
            // hero6BlackRadioButton
            // 
            this.hero6BlackRadioButton.AutoSize = true;
            this.hero6BlackRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero6BlackRadioButton.Location = new System.Drawing.Point(27, 118);
            this.hero6BlackRadioButton.Name = "hero6BlackRadioButton";
            this.hero6BlackRadioButton.Size = new System.Drawing.Size(209, 29);
            this.hero6BlackRadioButton.TabIndex = 5;
            this.hero6BlackRadioButton.Text = "GoPro Hero &6 Black";
            this.hero6BlackRadioButton.UseVisualStyleBackColor = true;
            this.hero6BlackRadioButton.CheckedChanged += new System.EventHandler(this.hero6BlackRadioButton_CheckedChanged);
            // 
            // hero7BlackRadioButton
            // 
            this.hero7BlackRadioButton.AutoSize = true;
            this.hero7BlackRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero7BlackRadioButton.Location = new System.Drawing.Point(296, 28);
            this.hero7BlackRadioButton.Name = "hero7BlackRadioButton";
            this.hero7BlackRadioButton.Size = new System.Drawing.Size(209, 29);
            this.hero7BlackRadioButton.TabIndex = 2;
            this.hero7BlackRadioButton.Text = "GoPro Hero &7 Black";
            this.hero7BlackRadioButton.UseVisualStyleBackColor = true;
            this.hero7BlackRadioButton.CheckedChanged += new System.EventHandler(this.hero7BlackRadioButton_CheckedChanged);
            // 
            // hero8BlackRadioButton
            // 
            this.hero8BlackRadioButton.AutoSize = true;
            this.hero8BlackRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero8BlackRadioButton.Location = new System.Drawing.Point(27, 28);
            this.hero8BlackRadioButton.Name = "hero8BlackRadioButton";
            this.hero8BlackRadioButton.Size = new System.Drawing.Size(209, 29);
            this.hero8BlackRadioButton.TabIndex = 1;
            this.hero8BlackRadioButton.Text = "GoPro Hero &8 Black";
            this.hero8BlackRadioButton.UseVisualStyleBackColor = true;
            this.hero8BlackRadioButton.CheckedChanged += new System.EventHandler(this.hero8BlackRadioButton_CheckedChanged);
            // 
            // orderCompleteToolStripMenuItem
            // 
            this.orderCompleteToolStripMenuItem.Enabled = false;
            this.orderCompleteToolStripMenuItem.Name = "orderCompleteToolStripMenuItem";
            this.orderCompleteToolStripMenuItem.Size = new System.Drawing.Size(242, 34);
            this.orderCompleteToolStripMenuItem.Text = "&Order Complete";
            this.orderCompleteToolStripMenuItem.Click += new System.EventHandler(this.orderCompleteButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1908, 33);
            this.menuStrip1.TabIndex = 58;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToOrderToolStripMenuItem,
            this.orderCompleteToolStripMenuItem,
            this.toolStripSeparator1,
            this.fontToolStripMenuItem,
            this.colourToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(58, 29);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // addToOrderToolStripMenuItem
            // 
            this.addToOrderToolStripMenuItem.Name = "addToOrderToolStripMenuItem";
            this.addToOrderToolStripMenuItem.Size = new System.Drawing.Size(242, 34);
            this.addToOrderToolStripMenuItem.Text = "&Add to Order";
            this.addToOrderToolStripMenuItem.Click += new System.EventHandler(this.addToOrderButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(239, 6);
            // 
            // promotionLabel
            // 
            this.promotionLabel.AutoSize = true;
            this.promotionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.promotionLabel.Location = new System.Drawing.Point(1153, 903);
            this.promotionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.promotionLabel.Name = "promotionLabel";
            this.promotionLabel.Size = new System.Drawing.Size(403, 22);
            this.promotionLabel.TabIndex = 54;
            this.promotionLabel.Text = "*Promotion code will be applied after the discount";
            // 
            // bestsellerGroupBox
            // 
            this.bestsellerGroupBox.Controls.Add(this.hero6BlackLabel);
            this.bestsellerGroupBox.Controls.Add(this.hero7BlackLabel);
            this.bestsellerGroupBox.Controls.Add(this.hero8BlackLabel);
            this.bestsellerGroupBox.Controls.Add(this.hero6BlackPictureBox);
            this.bestsellerGroupBox.Controls.Add(this.hero7BlackPictureBox);
            this.bestsellerGroupBox.Controls.Add(this.hero8BlackPictureBox);
            this.bestsellerGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bestsellerGroupBox.Location = new System.Drawing.Point(12, 653);
            this.bestsellerGroupBox.Name = "bestsellerGroupBox";
            this.bestsellerGroupBox.Size = new System.Drawing.Size(606, 204);
            this.bestsellerGroupBox.TabIndex = 53;
            this.bestsellerGroupBox.TabStop = false;
            this.bestsellerGroupBox.Text = "Bestsellers";
            // 
            // hero6BlackLabel
            // 
            this.hero6BlackLabel.AutoSize = true;
            this.hero6BlackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero6BlackLabel.Location = new System.Drawing.Point(408, 168);
            this.hero6BlackLabel.Name = "hero6BlackLabel";
            this.hero6BlackLabel.Size = new System.Drawing.Size(184, 25);
            this.hero6BlackLabel.TabIndex = 5;
            this.hero6BlackLabel.Text = "GoPro Hero 6 Blac&k";
            // 
            // hero7BlackLabel
            // 
            this.hero7BlackLabel.AutoSize = true;
            this.hero7BlackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero7BlackLabel.Location = new System.Drawing.Point(204, 168);
            this.hero7BlackLabel.Name = "hero7BlackLabel";
            this.hero7BlackLabel.Size = new System.Drawing.Size(184, 25);
            this.hero7BlackLabel.TabIndex = 4;
            this.hero7BlackLabel.Text = "GoPro Hero 7 B&lack";
            // 
            // hero8BlackLabel
            // 
            this.hero8BlackLabel.AutoSize = true;
            this.hero8BlackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hero8BlackLabel.Location = new System.Drawing.Point(4, 168);
            this.hero8BlackLabel.Name = "hero8BlackLabel";
            this.hero8BlackLabel.Size = new System.Drawing.Size(184, 25);
            this.hero8BlackLabel.TabIndex = 3;
            this.hero8BlackLabel.Text = "GoPro &Hero 8 Black";
            // 
            // hero6BlackPictureBox
            // 
            this.hero6BlackPictureBox.Image = global::Proj2_TI03_MereditheWan.Properties.Resources.Hero_6_Black;
            this.hero6BlackPictureBox.Location = new System.Drawing.Point(450, 38);
            this.hero6BlackPictureBox.Name = "hero6BlackPictureBox";
            this.hero6BlackPictureBox.Size = new System.Drawing.Size(140, 125);
            this.hero6BlackPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.hero6BlackPictureBox.TabIndex = 2;
            this.hero6BlackPictureBox.TabStop = false;
            // 
            // hero7BlackPictureBox
            // 
            this.hero7BlackPictureBox.Image = global::Proj2_TI03_MereditheWan.Properties.Resources.Hero_7_Black;
            this.hero7BlackPictureBox.Location = new System.Drawing.Point(248, 38);
            this.hero7BlackPictureBox.Name = "hero7BlackPictureBox";
            this.hero7BlackPictureBox.Size = new System.Drawing.Size(140, 125);
            this.hero7BlackPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.hero7BlackPictureBox.TabIndex = 1;
            this.hero7BlackPictureBox.TabStop = false;
            // 
            // hero8BlackPictureBox
            // 
            this.hero8BlackPictureBox.Image = global::Proj2_TI03_MereditheWan.Properties.Resources.Hero_8_Black;
            this.hero8BlackPictureBox.Location = new System.Drawing.Point(32, 38);
            this.hero8BlackPictureBox.Name = "hero8BlackPictureBox";
            this.hero8BlackPictureBox.Size = new System.Drawing.Size(140, 125);
            this.hero8BlackPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.hero8BlackPictureBox.TabIndex = 0;
            this.hero8BlackPictureBox.TabStop = false;
            // 
            // exitButton
            // 
            this.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(1684, 638);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(199, 55);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "E&xit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // promotionCodeLabel
            // 
            this.promotionCodeLabel.AutoSize = true;
            this.promotionCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.promotionCodeLabel.Location = new System.Drawing.Point(1152, 866);
            this.promotionCodeLabel.Name = "promotionCodeLabel";
            this.promotionCodeLabel.Size = new System.Drawing.Size(174, 25);
            this.promotionCodeLabel.TabIndex = 50;
            this.promotionCodeLabel.Text = "Promotion Code:";
            // 
            // addToOrderButton
            // 
            this.addToOrderButton.Enabled = false;
            this.addToOrderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addToOrderButton.Location = new System.Drawing.Point(1684, 103);
            this.addToOrderButton.Name = "addToOrderButton";
            this.addToOrderButton.Size = new System.Drawing.Size(199, 63);
            this.addToOrderButton.TabIndex = 1;
            this.addToOrderButton.Text = "&Add To Order";
            this.addToOrderButton.UseVisualStyleBackColor = true;
            this.addToOrderButton.Click += new System.EventHandler(this.addToOrderButton_Click);
            // 
            // checkOutButton
            // 
            this.checkOutButton.Enabled = false;
            this.checkOutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkOutButton.Location = new System.Drawing.Point(1684, 351);
            this.checkOutButton.Name = "checkOutButton";
            this.checkOutButton.Size = new System.Drawing.Size(199, 59);
            this.checkOutButton.TabIndex = 4;
            this.checkOutButton.Text = "Chec&k Out";
            this.checkOutButton.UseVisualStyleBackColor = true;
            this.checkOutButton.Click += new System.EventHandler(this.checkOutButton_Click);
            // 
            // promotionCodeTextBox
            // 
            this.promotionCodeTextBox.Enabled = false;
            this.promotionCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.promotionCodeTextBox.Location = new System.Drawing.Point(1369, 863);
            this.promotionCodeTextBox.Name = "promotionCodeTextBox";
            this.promotionCodeTextBox.Size = new System.Drawing.Size(187, 30);
            this.promotionCodeTextBox.TabIndex = 38;
            // 
            // orderSummaryGroupBox
            // 
            this.orderSummaryGroupBox.Controls.Add(this.orderListBox);
            this.orderSummaryGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderSummaryGroupBox.Location = new System.Drawing.Point(1146, 130);
            this.orderSummaryGroupBox.Name = "orderSummaryGroupBox";
            this.orderSummaryGroupBox.Size = new System.Drawing.Size(500, 386);
            this.orderSummaryGroupBox.TabIndex = 48;
            this.orderSummaryGroupBox.TabStop = false;
            this.orderSummaryGroupBox.Text = "Order Summary";
            // 
            // orderListBox
            // 
            this.orderListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderListBox.FormattingEnabled = true;
            this.orderListBox.ItemHeight = 22;
            this.orderListBox.Location = new System.Drawing.Point(0, 32);
            this.orderListBox.Name = "orderListBox";
            this.orderListBox.Size = new System.Drawing.Size(494, 334);
            this.orderListBox.TabIndex = 0;
            this.orderListBox.TabStop = false;
            this.orderListBox.SelectedIndexChanged += new System.EventHandler(this.orderListBox_SelectedIndexChanged);
            // 
            // productsLabel
            // 
            this.productsLabel.AutoSize = true;
            this.productsLabel.Font = new System.Drawing.Font("Segoe Print", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productsLabel.Location = new System.Drawing.Point(184, 42);
            this.productsLabel.Name = "productsLabel";
            this.productsLabel.Size = new System.Drawing.Size(280, 57);
            this.productsLabel.TabIndex = 42;
            this.productsLabel.Text = "GoPro Products";
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateLabel.Location = new System.Drawing.Point(1174, 79);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(53, 25);
            this.dateLabel.TabIndex = 59;
            this.dateLabel.Text = "Date";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.Location = new System.Drawing.Point(1462, 79);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(56, 25);
            this.timeLabel.TabIndex = 60;
            this.timeLabel.Text = "Time";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // discountPackagesGroupBox
            // 
            this.discountPackagesGroupBox.Controls.Add(this.dpCapLabel);
            this.discountPackagesGroupBox.Controls.Add(this.dp3Label);
            this.discountPackagesGroupBox.Controls.Add(this.dp2Label);
            this.discountPackagesGroupBox.Controls.Add(this.dp1Label);
            this.discountPackagesGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discountPackagesGroupBox.Location = new System.Drawing.Point(1146, 535);
            this.discountPackagesGroupBox.Name = "discountPackagesGroupBox";
            this.discountPackagesGroupBox.Size = new System.Drawing.Size(500, 167);
            this.discountPackagesGroupBox.TabIndex = 61;
            this.discountPackagesGroupBox.TabStop = false;
            this.discountPackagesGroupBox.Text = "Discount Packages";
            // 
            // dpCapLabel
            // 
            this.dpCapLabel.AutoSize = true;
            this.dpCapLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpCapLabel.Location = new System.Drawing.Point(15, 136);
            this.dpCapLabel.Name = "dpCapLabel";
            this.dpCapLabel.Size = new System.Drawing.Size(234, 22);
            this.dpCapLabel.TabIndex = 3;
            this.dpCapLabel.Text = "*Discount is capped at $300";
            // 
            // dp3Label
            // 
            this.dp3Label.AutoSize = true;
            this.dp3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dp3Label.Location = new System.Drawing.Point(15, 103);
            this.dp3Label.Name = "dp3Label";
            this.dp3Label.Size = new System.Drawing.Size(382, 22);
            this.dp3Label.TabIndex = 2;
            this.dp3Label.Text = "- 10% off storewide with min. spending of $400";
            // 
            // dp2Label
            // 
            this.dp2Label.AutoSize = true;
            this.dp2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dp2Label.Location = new System.Drawing.Point(15, 69);
            this.dp2Label.Name = "dp2Label";
            this.dp2Label.Size = new System.Drawing.Size(382, 22);
            this.dp2Label.TabIndex = 1;
            this.dp2Label.Text = "- 15% off storewide with min. spending of $550";
            // 
            // dp1Label
            // 
            this.dp1Label.AutoSize = true;
            this.dp1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dp1Label.Location = new System.Drawing.Point(15, 40);
            this.dp1Label.Name = "dp1Label";
            this.dp1Label.Size = new System.Drawing.Size(382, 22);
            this.dp1Label.TabIndex = 0;
            this.dp1Label.Text = "- 25% off storewide with min. spending of $680\r\n";
            // 
            // pc1Label
            // 
            this.pc1Label.AutoSize = true;
            this.pc1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pc1Label.Location = new System.Drawing.Point(15, 34);
            this.pc1Label.Name = "pc1Label";
            this.pc1Label.Size = new System.Drawing.Size(609, 22);
            this.pc1Label.TabIndex = 62;
            this.pc1Label.Text = "- “STAYHOME10” for $10 off with no min. spending, valid until 1 June 2020";
            // 
            // dc2Label
            // 
            this.dc2Label.AutoSize = true;
            this.dc2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dc2Label.Location = new System.Drawing.Point(15, 62);
            this.dc2Label.Name = "dc2Label";
            this.dc2Label.Size = new System.Drawing.Size(638, 22);
            this.dc2Label.TabIndex = 63;
            this.dc2Label.Text = "- “SPECIAL20” for $20 off, with min. spending of $180, valid until 15 June 2020";
            // 
            // pc3Label
            // 
            this.pc3Label.AutoSize = true;
            this.pc3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pc3Label.Location = new System.Drawing.Point(15, 91);
            this.pc3Label.Name = "pc3Label";
            this.pc3Label.Size = new System.Drawing.Size(701, 22);
            this.pc3Label.TabIndex = 64;
            this.pc3Label.Text = "- “NEWUSER30” for $30 off, valid for member’s first purchase until 28 December 20" +
    "20.";
            // 
            // promotionCodesGroupBox
            // 
            this.promotionCodesGroupBox.Controls.Add(this.pc1Label);
            this.promotionCodesGroupBox.Controls.Add(this.pc3Label);
            this.promotionCodesGroupBox.Controls.Add(this.dc2Label);
            this.promotionCodesGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.promotionCodesGroupBox.Location = new System.Drawing.Point(1146, 720);
            this.promotionCodesGroupBox.Name = "promotionCodesGroupBox";
            this.promotionCodesGroupBox.Size = new System.Drawing.Size(737, 126);
            this.promotionCodesGroupBox.TabIndex = 65;
            this.promotionCodesGroupBox.TabStop = false;
            this.promotionCodesGroupBox.Text = "Promotion Codes";
            // 
            // GoProForm
            // 
            this.AcceptButton = this.addToOrderButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.exitButton;
            this.ClientSize = new System.Drawing.Size(1908, 980);
            this.Controls.Add(this.promotionCodesGroupBox);
            this.Controls.Add(this.orderCompleteButton);
            this.Controls.Add(this.discountPackagesGroupBox);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.deleteSelectedItemButton);
            this.Controls.Add(this.cancelOrderButton);
            this.Controls.Add(this.summaryButton);
            this.Controls.Add(this.productInformationGroupBox);
            this.Controls.Add(this.maintenancePackageGroupBox);
            this.Controls.Add(this.addOnsGroupBox);
            this.Controls.Add(this.productSpecificationLabel);
            this.Controls.Add(this.goproGroupBox);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.promotionLabel);
            this.Controls.Add(this.bestsellerGroupBox);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.promotionCodeLabel);
            this.Controls.Add(this.addToOrderButton);
            this.Controls.Add(this.checkOutButton);
            this.Controls.Add(this.promotionCodeTextBox);
            this.Controls.Add(this.orderSummaryGroupBox);
            this.Controls.Add(this.productsLabel);
            this.Name = "GoProForm";
            this.Text = "GoPro ";
            this.Load += new System.EventHandler(this.GoProForm_Load);
            this.productInformationGroupBox.ResumeLayout(false);
            this.productInformationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productPictureBox)).EndInit();
            this.maintenancePackageGroupBox.ResumeLayout(false);
            this.maintenancePackageGroupBox.PerformLayout();
            this.addOnsGroupBox.ResumeLayout(false);
            this.addOnsGroupBox.PerformLayout();
            this.goproGroupBox.ResumeLayout(false);
            this.goproGroupBox.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.bestsellerGroupBox.ResumeLayout(false);
            this.bestsellerGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hero6BlackPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hero7BlackPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hero8BlackPictureBox)).EndInit();
            this.orderSummaryGroupBox.ResumeLayout(false);
            this.discountPackagesGroupBox.ResumeLayout(false);
            this.discountPackagesGroupBox.PerformLayout();
            this.promotionCodesGroupBox.ResumeLayout(false);
            this.promotionCodesGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton hero7WhiteRadioButton;
        private System.Windows.Forms.RadioButton noSelectionRadioButton;
        private System.Windows.Forms.CheckBox sdCardCheckBox;
        private System.Windows.Forms.CheckBox caseyCheckBox;
        private System.Windows.Forms.CheckBox threeWayCheckBox;
        private System.Windows.Forms.RadioButton invisibleRadioButton;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem businessReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button deleteSelectedItemButton;
        private System.Windows.Forms.Button cancelOrderButton;
        private System.Windows.Forms.Button summaryButton;
        private System.Windows.Forms.Button orderCompleteButton;
        private System.Windows.Forms.GroupBox productInformationGroupBox;
        private System.Windows.Forms.RichTextBox specificationRichTextBox;
        private System.Windows.Forms.Label productImageLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Label specificationLabel;
        private System.Windows.Forms.PictureBox productPictureBox;
        private System.Windows.Forms.CheckBox imageVisibleCheckBox;
        private System.Windows.Forms.GroupBox maintenancePackageGroupBox;
        private System.Windows.Forms.RadioButton premiumPackageRadioButton;
        private System.Windows.Forms.RadioButton advancedPackageRadioButton;
        private System.Windows.Forms.RadioButton basicPackageRadioButton;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox addOnsGroupBox;
        private System.Windows.Forms.ToolStripMenuItem colourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem;
        private System.Windows.Forms.Label productSpecificationLabel;
        private System.Windows.Forms.GroupBox goproGroupBox;
        private System.Windows.Forms.RadioButton hero7SilverRadioButton;
        private System.Windows.Forms.RadioButton hero5BlackRadioButton;
        private System.Windows.Forms.RadioButton hero6BlackRadioButton;
        private System.Windows.Forms.RadioButton hero7BlackRadioButton;
        private System.Windows.Forms.RadioButton hero8BlackRadioButton;
        private System.Windows.Forms.ToolStripMenuItem orderCompleteToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label promotionLabel;
        private System.Windows.Forms.GroupBox bestsellerGroupBox;
        private System.Windows.Forms.Label hero6BlackLabel;
        private System.Windows.Forms.Label hero7BlackLabel;
        private System.Windows.Forms.Label hero8BlackLabel;
        private System.Windows.Forms.PictureBox hero6BlackPictureBox;
        private System.Windows.Forms.PictureBox hero7BlackPictureBox;
        private System.Windows.Forms.PictureBox hero8BlackPictureBox;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label promotionCodeLabel;
        private System.Windows.Forms.Button addToOrderButton;
        private System.Windows.Forms.Button checkOutButton;
        private System.Windows.Forms.TextBox promotionCodeTextBox;
        private System.Windows.Forms.GroupBox orderSummaryGroupBox;
        private System.Windows.Forms.Label productsLabel;
        private System.Windows.Forms.ListBox orderListBox;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox discountPackagesGroupBox;
        private System.Windows.Forms.Label dpCapLabel;
        private System.Windows.Forms.Label dp3Label;
        private System.Windows.Forms.Label dp2Label;
        private System.Windows.Forms.Label dp1Label;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.Label pc1Label;
        private System.Windows.Forms.Label dc2Label;
        private System.Windows.Forms.Label pc3Label;
        private System.Windows.Forms.GroupBox promotionCodesGroupBox;
        private System.Windows.Forms.Label label1;
    }
}

