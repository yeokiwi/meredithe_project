﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_TI03_MereditheWan
{
    public partial class BusinessReport : Form
    {
        int intCamera, intOrder, intMaintenance;
        decimal decTotalSales;
        string Camerabest;
        string AddOnbest;
        string Maintenancebest;
        public BusinessReport(int itemInteger, int orderInteger, int maintenanceInteger, decimal totalSalesDecimal, string bestCamera, string bestAddOn, string bestMaintenance)
        {
            InitializeComponent();
            intCamera = itemInteger;
            intOrder = orderInteger;
            intMaintenance = maintenanceInteger;
            decTotalSales = totalSalesDecimal;
            Camerabest = bestCamera;
            AddOnbest = bestAddOn;
            Maintenancebest = bestMaintenance;
        }

        private void transactionTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void enterButton_Click(object sender, EventArgs e)
        {
            if(passwordTextBox.Text == "")
            {
                MessageBox.Show("Please enter password.");
            }
            else if (passwordTextBox.Text == "goproadmin28")
            {
                dailyBusinessReportGroupBox.Enabled = true;
                dailyBusinessReportGroupBox.Visible = true;
                staffAccessGroupBox.Enabled = false;
            }
            else
            {
                MessageBox.Show("Wrong password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void BusinessReport_Load(object sender, EventArgs e)
        {
            transactionTextBox.Text = intOrder.ToString();
            totalSalesTextBox.Text = decTotalSales.ToString("C");
            totalGoProTextBox.Text = intCamera.ToString();
            popGoProTextBox.Text = Camerabest;
            popAddOnTextBox.Text = AddOnbest;
            maintenanceTextBox.Text = Maintenancebest;
        }
    }
}
